package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/alexflint/go-arg"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

const ApiUrl = "https://api.telegram.org/"

type ParseMode string

const (
	HtmlParseMode       ParseMode = "HTML"
	MarkdownV2ParseMode ParseMode = "MarkdownV2"
)

var config struct {
	Message   string    `arg:"required,positional" help:"Message for send"`
	BotId     string    `arg:"--bot-id,env:BOT_ID,required" help:"Telegram Bot ID"`
	ChatIds   []string  `arg:"--chat-ids,env:CHAT_IDS,required" help:"Telegram Chat IDs"`
	ParseMode ParseMode `arg:"--parse-mode,env:PARSE_MODE" help:"Parse mode for message string[HTML, MarkdownV2]"`
	Retries   int       `arg:"--retries,env:Retries" help:"Number of attempts to send a message" `
}

func getBotUrl() string {
	return ApiUrl + "bot" + config.BotId
}

func getCommandUrl(command string) string {
	return getBotUrl() + "/" + command
}

func sendMessage(message string, parseMode ParseMode) {
	url := getCommandUrl("sendMessage")
	message = strings.Replace(message, `\n`, "\n", -1)
	for _, chatId := range config.ChatIds {
		values := map[string]string{
			"chat_id":    chatId,
			"text":       message,
			"parse_mode": string(parseMode),
		}
		jsonValue, _ := json.Marshal(values)
		for r := 1; r <= config.Retries; r++ {
			resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
			if err != nil {
				errStr := fmt.Sprintf("Error on send telegram message: %s", err)
				if r == config.Retries {
					log.Fatalln(errStr)
				} else {
					log.Println(errStr)
					continue
				}
			}
			if resp.StatusCode >= 400 {
				body, _ := ioutil.ReadAll(resp.Body)
				errStr := fmt.Sprintf("Error on send telegram message: %s: %s", resp.Status, string(body))
				if r == config.Retries {
					log.Fatalln(errStr)
				} else {
					log.Println(errStr)
					continue
				}
			}
			break
		}
	}
}

func main() {
	config.ParseMode = HtmlParseMode
	config.Retries = 3
	arg.MustParse(&config)
	sendMessage(config.Message, config.ParseMode)
}

# Telegram notifier
Simple cli tool for send messages throw telegram bot.

## Usage
```shell script
telegram_notifier --bot-id BOT-ID --chat-ids CHAT-IDS [--parse-mode PARSE-MODE] [--retries RETRIES] MESSAGE

Positional arguments:
  MESSAGE                Message for send

Options:
  --bot-id BOT-ID        Telegram Bot ID
  --chat-ids CHAT-IDS    Telegram Chat IDs
  --parse-mode PARSE-MODE
                         Parse mode for message string[HTML, MarkdownV2] [default: HTML]
  --retries RETRIES      Number of attempts to send a message [default: 3]
  --help, -h             display this help and exit
```
You can use '\n' in message for line breaks.

## Environment vars
BOT_ID   
CHAT_IDS  
PARSE_MODE


## TODO 
* Fix link for release asset: 
https://gitlab.com/api/v4/projects/22543844/packages/generic/telegram_notifier/0.1.2/telegram-notifier-linux-amd64
to
https://gitlab.com/smartgeosystem/utilities/telegram_notifier/-/package_files/4762815/download
* Example in help
* Env vars in help
